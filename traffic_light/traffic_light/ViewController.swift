//
//  ViewController.swift
//  traffic_light
//
//  Created by Janis  on 15.02.18.
//  Copyright © 2018. g. janisengers. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    let green   = UIColor(hue: 0.2528, saturation: 1, brightness: 0.66, alpha: 1.0)
    let orange  = UIColor(hue: 0.1389, saturation: 1, brightness: 0.99, alpha: 1.0)
    let red     = UIColor(hue: 0, saturation: 1, brightness: 1, alpha: 1.0)
    
    let trafficLightStarts  = "06:00"
    let trafficLightEnds    = "00:00"
    let midnight            = 1440
    
    let greenDuration   = 2
    let orangeDuration  = 1
    let redDuration     = 3
    
    let dateFormatter = DateFormatter()
    
    var cycleDuration       = 0
    var startTimeInMinutes  = 0
    var endTimeInMinutes    = 0
    
    @IBOutlet weak var selectedTimeDatePicker: UIDatePicker!
    @IBOutlet weak var lightCircleView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dateFormatter.dateFormat = "HH:mm"
        
        cycleDuration       = greenDuration + (orangeDuration * 2) + redDuration
        startTimeInMinutes  = timeStringToMinutes(timeStr: trafficLightStarts)
        endTimeInMinutes    = timeStringToMinutes(timeStr: trafficLightEnds)

        lightCircleView.layer.cornerRadius  = lightCircleView.frame.size.width/2
        lightCircleView.clipsToBounds       = true
        lightCircleView.layer.borderColor   = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lightCircleView.layer.borderWidth   = 5.0
        
        setTrafficLightState(state: calculateTrafficLightState())
    }

    @IBAction func selectedTimeDatePickerAction()
    {
        setTrafficLightState(state: calculateTrafficLightState())
    }
    
    func setTrafficLightState(state: Int)
    {
        lightCircleView.isHidden    = false
        messageLabel.isHidden       = true
        
        switch state {
            case 1:
                lightCircleView.backgroundColor = green
            case 2:
                lightCircleView.backgroundColor = orange
            case 3:
                lightCircleView.backgroundColor = red
            default:
                lightCircleView.isHidden    = true
                messageLabel.isHidden       = false
        }
    }
    
    func calculateTrafficLightState()->Int
    {
        let selectedTimeInMinutes = timeStringToMinutes(timeStr: dateFormatter.string(from: selectedTimeDatePicker.date))
        
        let currentMinuteInCycle: Int
        
        if startTimeInMinutes < endTimeInMinutes
        {
            if selectedTimeInMinutes < startTimeInMinutes || selectedTimeInMinutes > endTimeInMinutes
            {
                return 0
            }
            else
            {
                currentMinuteInCycle = (selectedTimeInMinutes - startTimeInMinutes)  % cycleDuration
            }
        }
        else
        {
            if selectedTimeInMinutes < startTimeInMinutes && selectedTimeInMinutes >= endTimeInMinutes
            {
                return 0
            }
            else
            {
                currentMinuteInCycle = (selectedTimeInMinutes >= startTimeInMinutes)
                    ? (selectedTimeInMinutes - startTimeInMinutes)  % cycleDuration
                    : (midnight - startTimeInMinutes + selectedTimeInMinutes)  % cycleDuration
            }
        }
        
        if currentMinuteInCycle < greenDuration
        {
            return 1
        }
        else if currentMinuteInCycle >= greenDuration + orangeDuration && currentMinuteInCycle < cycleDuration - orangeDuration
        {
            return 3
        }
        else
        {
            return 2
        }
    }
    
    func timeStringToMinutes(timeStr: String) -> Int
    {
        let time   = timeStr.split(separator: ":").map(String.init)
        
        let h = Int(time[0])!
        let m = Int(time[1])!
        
        let minutes = h * 60 + m;
        
        return minutes
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

